
<?php defined('BASEPATH') OR exit('No direct script access allowed');
require APPPATH . '/libraries/API_Controller.php';
class User_Api extends API_Controller
{
    public function __construct()
    {
        parent::__construct();
    }

    public function simple_api()
    {

        header("Access-Control-Allow-Origin: *");

        // API CONFIG
        $this->_apiConfig(array(
            'methods' => array('POST', 'GET')
        ));

    }

    public function api_limit()
    {
        /**
         * API Limit
         * ----------------------------------
         * @param: {int} API limit Number
         * @param: {string} API limit Type (IP)
         * @param: {int} API limit Time [minute]
         */

        $this->_APIConfig(array(
            // number limit, type limit, time limit (last minute)
            'limit' => array(50, 'ip', 'everyday'),
        ));
    }

    public function api_key(){
        /**
         * Use API Key without Database
         * ---------------------------------------------------------
         * @param: {string} Types
         * @param: {string} API Key
         */
        $this->_APIConfig(array(
            'methods' => array('POST'),
            //'key' => array('header', '13d1266d-81bd-4dc4-920f-eab672a802df'),

            // API Key With Database
            'key' => array('header'),

            // Add Custom data in API Response
            'data' => array(
                'is_login' => false
            ),

        ));

        // Data
        $data = 0;

        /**
         * Return API Response
         * ---------------------------------------------------------
         * @param: API Data
         * @param: Request Status Code
         */
        if (!empty($data)){
            $this->api_return($data, '200');
        }else{
            $this->api_return(array('status' => false, 'error' => 'Invalid Data'), '404');
        }

    }

    public function login(){
        header("Access-Control-Allow-Origin: *");

        // API Configuration
        $this->_apiConfig(array(
            'methods' => array('POST'),
        ));

        // you user authentication code will go here, you can compare the user with the database or whatever
        $payload = array(
            'id' => "Your User's ID",
            'other' => "Some other data",
        );

        // Load Authorization Library or Load in autoload config file
        $this->load->library('authorization_token');

        // generate a token
        $token = $this->authorization_token->generateToken($payload);

        // return data
        $this->api_return(
            array(
                'status' => true,
                "result" => array(
                    'token' => $token,
                ),

            ),
            200);
    }

    /**
     * view method
     *
     * @link [api/user/view]
     * @method POST
     * @return Response|void
     */
    public function view()
    {
        header("Access-Control-Allow-Origin: *");

        // API Configuration [Return Array: User Token Data]
        $user_data = $this->_apiConfig(array(
            'methods' => array('POST'),
            'requireAuthorization' => true,
        ));

        //die(var_dump($user_data));

        // return data
        $this->api_return(
            array(
                'status' => true,
                "result" => array(
                    'user_data' => $user_data['token_data']
                ),
            ),
            200);
    }

    public function movies() {

        header('Access-Control-Allow-Origin: *');

        $this->_APIConfig(
            array(
               'methods' => array('GET', 'POST'),
               'requireAuthorization' => true
            )
        );

        $requestType = $this->input->server('REQUEST_METHOD');

        switch ($requestType) {

            case 'GET' :
                $movies = $this->db->select('*') // Selection des Champs
                    ->from('movies') // Depuis la table movies
                    ->get() // Execution de la requête
                    ->result(); // Retour d'un tableau en Object

                $this->api_return($movies, '200');
                break;

            case 'POST' :
                // Récupération de tous les champs postés
                $data['mov_name'] = $this->input->post('mov_name');
                $data['mov_year'] = $this->input->post('mov_year');
                $data['mov_img'] = $this->input->post('mov_img');
                $data['mov_rating'] = $this->input->post('mov_rating');
                $data['mov_link'] = $this->input->post('mov_link');
                $data['mov_description'] = $this->input->post('mov_description');

                // On récupère le 4eme segment dans l'url
                $id = $this->uri->segment(4);
                die(var_dump($id));

                if ($id) {

                }

                $this->db->insert('movies', $data);

                if ($this->db->affected_rows() > 0) {

                    $response = array(
                        'Success' => 'Votre film a été ajouté avec succès !'
                    );

                } else {
                    $response = array(
                        'Error' => 'Un problème est survenu lors de l\'ajout du film'
                    );
                }

                $this->api_return($response, '200');
                break;



        }

        //die(var_dump($requestType));
    }
}
